from datetime import timedelta
import pandas as pd

def userSubscipritonData(db):

    # assume you have a "long-form" data frame
    # see https://plotly.com/python/px-arguments/ for more options
    SQL = f"""
    SELECT  payment_approved_items.user_id, payment_approved_items.price,
    payment_approvals.pg_provider, payment_approved_items.licensed_key
    ,payment_approved_items.end_date, date_trunc('day', payment_approved_items.created_at) as created_at
    FROM payment_approved_items
    LEFT JOIN payment_approvals ON 
        payment_approvals.id = payment_approved_items.approval_id
    LEFT JOIN users ON 
        users.id = payment_approved_items.user_id
    LEFT JOIN payment_subscription_products ON
        payment_subscription_products.name = payment_approved_items.licensed_key
    WHERE payment_approved_items.item_type = 'Payment::SubscriptionProduct'
    AND payment_approved_items.refund_id is null
    AND users.role != 16
    AND users.role != 8
    AND payment_approved_items.price != 0
        """
    read_user_subscription = pd.read_sql(SQL, db).sort_values('user_id')

    #결제 수단이 paypal인 경우 달려에서 원으로 환산합니다.
    WON_to_DDOLLAR = 1100
    paypal_index = read_user_subscription[read_user_subscription['pg_provider'] =='paypal'].index
    read_user_subscription.loc[paypal_index,'price'] = read_user_subscription.loc[paypal_index,'price'] * WON_to_DDOLLAR
    read_user_subscription = read_user_subscription.loc[:,['user_id', 'price', 'licensed_key','end_date', 'created_at']].drop_duplicates()

    personal_idx = read_user_subscription[read_user_subscription['licensed_key'] == '무한브금99'].index
    standart_idx = read_user_subscription[(read_user_subscription['licensed_key'] == '브금+효과음 무제한Biz')
                                         | (read_user_subscription['licensed_key'] == '무한브금Biz')
                                         | (read_user_subscription['licensed_key'] == '브금무제한Biz - 오프라인')
                                         | (read_user_subscription['licensed_key'] == '무한브금Biz비영리')
                                         | (read_user_subscription['licensed_key'] == '무한브금Biz - 1년')
                                         | (read_user_subscription['licensed_key'] == '무한브금Biz - 외주, 납품')
                                         | (read_user_subscription['licensed_key'] == '무한브금Biz - 선거용')].index
    premium_idx = read_user_subscription[(read_user_subscription['licensed_key'] == '프리미엄 멤버십')
                                        | (read_user_subscription['licensed_key'] == '프리미엄 관공서 멤버십 - 4개월')
                                        | (read_user_subscription['licensed_key'] == '프리미엄 멤버십 - 5개월')
                                        | (read_user_subscription['licensed_key'] == '무한브금Pro')].index

    read_user_subscription.loc[personal_idx,'licensed_key'] = '퍼스널 멤버십'
    read_user_subscription.loc[standart_idx,'licensed_key'] = '스탠다드 멤버십'
    read_user_subscription.loc[premium_idx,'licensed_key'] = '프리미엄 멤버십'
    read_user_subscription['end_date'] = read_user_subscription['end_date'] + timedelta(days=1)

    return read_user_subscription


def readPackageData(db):
    # 최근 한달 구독, 패키지 데이
    SQL = f"""
    SELECT  payment_approved_items.user_id, payment_approved_items.price,
    payment_approvals.pg_provider, payment_approved_items.licensed_key, payment_approved_items.item_name, payment_approved_items.item_type
    ,payment_approved_items.end_date, date_trunc('day', payment_approved_items.created_at) as created_at
    FROM payment_approved_items
    LEFT JOIN payment_approvals ON 
    	payment_approvals.id = payment_approved_items.approval_id
    LEFT JOIN users ON 
        users.id = payment_approved_items.user_id
    LEFT JOIN payment_subscription_products ON
        payment_subscription_products.name = payment_approved_items.licensed_key
    WHERE (payment_approved_items.item_type = 'Payment::SubscriptionProduct'
    OR payment_approved_items.item_type = 'Classify::Package')
    AND payment_approved_items.refund_id is null
    AND users.role != 16
    AND users.role != 8
    AND payment_approved_items.price != 0
    AND payment_approved_items.created_at > (CURRENT_DATE - 30)
        """
    read_user_subscription = pd.read_sql(SQL, db).sort_values('user_id')
    # 결제 수단이 paypal인 경우 달려에서 원으로 환산합니다.
    WON_to_DDOLLAR = 1100
    paypal_index = read_user_subscription[read_user_subscription['pg_provider'] == 'paypal'].index
    read_user_subscription.loc[paypal_index, 'price'] = read_user_subscription.loc[
                                                            paypal_index, 'price'] * WON_to_DDOLLAR

    personal_idx = read_user_subscription[read_user_subscription['item_name'] == '무한브금99'].index
    standart_idx = read_user_subscription[(read_user_subscription['item_name'] == '브금+효과음 무제한Biz')
                                          | (read_user_subscription['item_name'] == '무한브금Biz')
                                          | (read_user_subscription['item_name'] == '브금무제한Biz - 오프라인')
                                          | (read_user_subscription['item_name'] == '무한브금Biz비영리')
                                          | (read_user_subscription['item_name'] == '무한브금Biz - 1년')
                                          | (read_user_subscription['item_name'] == '무한브금Biz - 외주, 납품')
                                          | (read_user_subscription['item_name'] == '무한브금Biz - 선거용')].index
    premium_idx = read_user_subscription[(read_user_subscription['item_name'] == '프리미엄 멤버십')
                                         | (read_user_subscription['item_name'] == '프리미엄 관공서 멤버십 - 4개월')
                                         | (read_user_subscription['item_name'] == '프리미엄 멤버십 - 5개월')
                                         | (read_user_subscription['item_name'] == '무한브금Pro')].index

    read_user_subscription.loc[personal_idx, 'item_name'] = '퍼스널 멤버십'
    read_user_subscription.loc[standart_idx, 'item_name'] = '스탠다드 멤버십'
    read_user_subscription.loc[premium_idx, 'item_name'] = '프리미엄 멤버십'
    read_user_subscription.loc[personal_idx, 'licensed_key'] = '퍼스널 멤버십'
    read_user_subscription.loc[standart_idx, 'licensed_key'] = '스탠다드 멤버십'
    read_user_subscription.loc[premium_idx, 'licensed_key'] = '프리미엄 멤버십'

    package_index = read_user_subscription[read_user_subscription['item_type'] == 'Classify::Package'].index
    read_user_subscription.loc[package_index, 'licensed_key'] = '패키지'

    read_user_subscription = read_user_subscription.loc[:, ['user_id', 'price', 'licensed_key', 'item_name', 'end_date',
                                                            'created_at']].drop_duplicates()

    return read_user_subscription