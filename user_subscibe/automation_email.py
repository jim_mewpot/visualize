from database import DB
import pandas as pd

database = DB()
database.connect()

SQL = """
SELECT "username", "email", "last_sign_in_at"
FROM users
WHERE "email_acceptance" = 'true'
And "email" NOT LIKE '%@kakao.com%'
AND "email" NOT LIKE '%@pages.plusgoogle.com%'
AND "role" != 16
ANd "role" != 8
AND "last_sign_in_at" IS NOT NULL
ORDER BY "last_sign_in_at" DESC
"""


df = pd.read_sql(SQL,database.connect()[1])
print(df)
