import dash_core_components as dcc
import dash_html_components as html
from user_subscription_data import userSubscipritonData
import numpy as np
import dash_table
import datetime
import pandas as pd
from database.database import db

read_user_subscription = userSubscipritonData(db)
graph_list3 = ['충성고객_테이블']

read_user_subscription['subscription_month'] = ((read_user_subscription['end_date'] - read_user_subscription['created_at']) / np.timedelta64(1,'M')).round(0)
grouped_user_subscription = read_user_subscription[read_user_subscription['price'] != 0].groupby(
    ['user_id', 'licensed_key']).agg({
    'price': 'sum',
    'end_date': 'max',
    'created_at': 'min',
    'subscription_month': 'sum'}).reset_index()
user_subscription_describe = grouped_user_subscription.loc[:, ['licensed_key', 'subscription_month']].groupby(
    'licensed_key').describe()

standard_subscription_month = user_subscription_describe.loc['스탠다드 멤버십', ('subscription_month', '75%')]
personal_subscription_month = user_subscription_describe.loc['퍼스널 멤버십', ('subscription_month', '75%')]
premium_subscription_month = user_subscription_describe.loc['프리미엄 멤버십', ('subscription_month', '75%')]

current_subscription_user = grouped_user_subscription[
    grouped_user_subscription['end_date'] > datetime.datetime.now()]

personal_VIP_users = \
current_subscription_user[(current_subscription_user['subscription_month'] >= personal_subscription_month) &
                          (current_subscription_user['licensed_key'] == '퍼스널 멤버십')]['user_id'].to_list()
standard_VIP_users = \
current_subscription_user[(current_subscription_user['subscription_month'] >= standard_subscription_month) &
                          (current_subscription_user['licensed_key'] == '스탠다드 멤버십')]['user_id'].to_list()
premium_VIP_users = \
current_subscription_user[(current_subscription_user['subscription_month'] >= premium_subscription_month) &
                          (current_subscription_user['licensed_key'] == '프리미엄 멤버십')]['user_id'].to_list()

SQL = f"""
SELECT 
    users.id,
    users.username
    ,date_trunc('day', users.created_at) as created_at
    ,date_trunc('day', users.last_sign_in_at) as last_sign_in_at
    ,users.email
    ,users.platform_usage ->'video' AS video
    ,users.platform_usage->'platform'->'youtube' AS youtube
    ,users.platform_usage->'platform'->'instagram' AS instagram
    ,users.platform_usage->'platform'->'facebook' AS facebook
    ,users.platform_usage->'platform'->'others' AS others
FROM users
    """

VIP_users = pd.read_sql(SQL, db)

user_data = VIP_users.merge(grouped_user_subscription, left_on='id', right_on='user_id', how='right')

user_data.rename(columns={'licensed_key': 'membership',
                          'end_date': 'subscription_end_date',
                          'price': 'sales',
                          'subscription_month': 'subscription_total_month',
                          'created_at_y': 'subscription_start_date',
                          'created_at_x': 'id_created_at'}, inplace=True)

user_data = user_data.iloc[:, [0, 1, 3, 4, 5, 6, 7, 8,9, 10, 11, 12, 13, 14,15]]
user_data = user_data[['id', 'username', 'email', 'membership', 'sales','last_sign_in_at', 'subscription_start_date','subscription_end_date',
                        'subscription_total_month', 'video', 'youtube', 'instagram',
                       'facebook', 'others']].set_index('id')

user_data.rename(columns={'username': '닉네임',
                          'email': 'email',
                          'membership': 'membership',
                          'last_sign_in_at' : '마지막 로그인 날짜',
                          'sales': '매출',
                          'subscription_start_date': '최초구독일',
                          'subscription_end_date': '구독 종료 예정일',
                          'subscription_total_month' : '구독 개월 수'}, inplace=True)

total_VIP_users = personal_VIP_users + standard_VIP_users + premium_VIP_users

VIP_users_df = user_data.loc[total_VIP_users, :].reset_index()
VIP_users_df['video'] = VIP_users_df['video'].apply(lambda x: str(x)[1:-1] if x != None else x)
layout3 =  html.Div(children=[
    html.H1(children='현재 구독 중인 유저 중 VIP(상위25%) 고객 목록'),
    dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in VIP_users_df.columns],
    data=VIP_users_df.to_dict('records'),
    export_format='xlsx',
    export_headers='display',
    filter_action='native',
    sort_action='native'
    ),
    html.Br(),
    dcc.Link('이전 페이지로 이동', href='/users'),
    html.Br(),
    dcc.Link('home', href='/')
    ])