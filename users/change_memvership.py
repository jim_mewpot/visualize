import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from user_subscription_data import userSubscipritonData
import plotly.express as px
from database.database import db
from main import app
import datetime
import numpy as np

read_user_subscription = userSubscipritonData(db)

layout2 = html.Div([
dcc.DatePickerRange(
    id='user_membership_change_trend',  # ID to be used for callback
    min_date_allowed=read_user_subscription['created_at'].min().date(),
    max_date_allowed=read_user_subscription['created_at'].max().date(),
    initial_visible_month=datetime.datetime.today().date(),
    end_date=read_user_subscription['created_at'].max().date()
    ),
    dcc.Graph(id='change_memvership'),
    html.Br(),
    dcc.Link('이전 페이지로 이동', href='/users'),
    html.Br(),
    dcc.Link('home', href='/'),
])

@app.callback(
    Output('change_memvership', 'figure'),
    [Input('user_membership_change_trend', 'start_date'),
     Input('user_membership_change_trend', 'end_date')]
)

def changeMembershipUsersTrend(start_date,end_date):
    # 구독자 변경 경험이 있는 경우 요즘제 변환 추이
    read_user_subscription['subscription_month'] = (
            (read_user_subscription['end_date'] - read_user_subscription['created_at']) / np.timedelta64(1, 'M')).round(0)
    grouped_user_subscription = read_user_subscription.groupby(['user_id', 'licensed_key']).agg({
        'price': 'sum',
        'end_date': 'max',
        'created_at': 'min',
        'subscription_month': 'sum'}).reset_index()
    changed_membership = grouped_user_subscription.groupby('user_id').count().sort_values(by='price')
    changed_membership_users = changed_membership[changed_membership['licensed_key'] >= 2].index
    # 요금제 변경이 있었던 케이스
    changed_membership_users_df = grouped_user_subscription.set_index('user_id').loc[changed_membership_users,
                                  :].reset_index()
    changed_membership_users_df['user_id'] = changed_membership_users_df['user_id'].astype('str')


    selected_data = changed_membership_users_df.set_index('created_at').loc[start_date:end_date].reset_index()
    selected_data['user_id'] = selected_data['user_id'].astype('str')
    selected_data['user_id'] = selected_data['user_id'].apply(lambda x: 'id :' + x)
    fig = px.timeline(selected_data, x_start="created_at", x_end="end_date", y="user_id",
                      color="licensed_key")
    fig.update_layout(width=1400, height=700)
    return fig
