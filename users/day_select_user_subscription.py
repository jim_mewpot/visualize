import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from user_subscription_data import userSubscipritonData
import plotly.express as px
from database.database import db
from main import app
import datetime

read_user_subscription = userSubscipritonData(db)


day_select_user_subscription_layout = html.Div([
dcc.DatePickerRange(
        id='datepicker',  # ID to be used for callback
    min_date_allowed=read_user_subscription['created_at'].min().date(),
    max_date_allowed=read_user_subscription['created_at'].max().date(),
    initial_visible_month=datetime.datetime.today().date(),
    end_date=read_user_subscription['created_at'].max().date()
    ),
    dcc.Graph(id='graph_datepicker'),
    html.Br(),
    dcc.Link('이전 페이지로 이동', href='/users'),
    html.Br(),
    dcc.Link('home', href='/'),
])

@app.callback(
    Output('graph_datepicker', 'figure'),
    [Input('datepicker', 'start_date'),
     Input('datepicker', 'end_date')]
)

def drawUserSubsciption(start_date, end_date):
    grouped_data = read_user_subscription[['created_at', 'licensed_key', 'user_id']].groupby(
        ['created_at', 'licensed_key']).count().reset_index().set_index('created_at')

    selected_data = grouped_data.loc[start_date:end_date].reset_index()
    selected_data = selected_data[['licensed_key', 'user_id']].groupby(['licensed_key']).sum().reset_index()

    fig = px.bar(selected_data, x='licensed_key', y='user_id', title="무료 체험을 제외한 판매 내역", color='licensed_key')
    fig.update_layout(width=1400, height=700)
    return fig
