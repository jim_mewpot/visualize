import dash
import dash_html_components as html
import dash_core_components as dcc


# app = dash.Dash(__name__, suppress_callback_exceptions=True)
# server = app.server

app_layout =  html.Div(children=[
    html.H3('Users'),
    html.Br(),
    dcc.Link('일별 구독자 추이', href='/users/daily_subscribtion'),
    html.Br(),
    dcc.Link('기간 선택 가능한 구독 총합', href='/users/day_select_user_subscription'),
    html.Br(),
    dcc.Link('기간 선택 가능한 패키지 총합', href='/users/day_select_user_package'),
    html.Br(),
    dcc.Link('구독자 변경 경험이 있는 경우 요즘제 변환 추이', href='/users/change_memvership'),
    html.Br(),
    dcc.Link('현재 구독 중인 유저 중 VIP(상위25%) 고객 목록', href='/users/VIP'),
    html.Br(),
    dcc.Link('해지 고객', href='/users/termination'),
    html.Br(),
    dcc.Link('home', href='/')
])