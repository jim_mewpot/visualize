import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from user_subscription_data import userSubscipritonData
from user_last30day_data import readUsersLast30DayData
import plotly.express as px
from database.database import db
from main import app
from datetime import date, timedelta
yesterday = (date.today() - timedelta(days=1)).strftime('%Y-%m-%d')

read_user_subscription = userSubscipritonData(db)
last_30_day = readUsersLast30DayData(db)
membership_data = last_30_day[last_30_day['licensed_key'] != '패키지']
package_data = last_30_day[last_30_day['licensed_key'] == '패키지']
month_membership = membership_data.loc[:,['licensed_key','user_id']].groupby('licensed_key').count().rename(columns = {'user_id' : 'count'}).reset_index()
month_package = package_data.loc[:,['item_name','user_id']].groupby('item_name').count().rename(columns = {'user_id' : 'count'}).reset_index()

graph_list = ['일별 구독자 추이','최근 한달 판매 된 멤버십','최근 한달 판매 된 패키지', '어제 판매 된 내역']
layout1 = html.Div(children=[
    html.H1(children='패키지, 멤버십 구입정보'),
    html.Div(
        [
            dcc.Dropdown(
                id="graph_list",
                options=[{
                    'label': i,
                    'value': i
                } for i in graph_list],
                value='Managers'),
        ],
        style={'width': '25%',
               'display': 'inline-block'}),
    dcc.Graph(id='graph'),
    html.Br(),
    dcc.Link('이전 페이지로 이동', href='/users'),
    html.Br(),
    dcc.Link('home', href='/'),
])

@app.callback(Output('graph', 'figure'),
                  [Input('graph_list', 'value')])

def drawUserSubsciption(graph_list):
    if graph_list == '일별 구독자 추이':
        start_count = read_user_subscription.loc[:, ['user_id', 'created_at', 'licensed_key']].groupby(
            ['created_at', 'licensed_key']).count().reset_index()
        start_count.rename(columns={'user_id': 'start_count'}, inplace=True)
        end_count = read_user_subscription.loc[:, ['user_id', 'end_date', 'licensed_key']].groupby(
            ['end_date', 'licensed_key']).count().reset_index()
        end_count.rename(columns={'user_id': 'end_count'}, inplace=True)
        result_count = start_count.merge(end_count, left_on=['licensed_key', 'created_at'],
                                         right_on=['licensed_key', 'end_date'], how='left').iloc[:,
                       [0, 1, 2, 4]].fillna(0)
        result_count['difference_count'] = result_count['start_count'] - result_count['end_count']
        result_count['cumsum_user_subscription'] = result_count.groupby('licensed_key').cumsum().loc[:,
                                                   ['difference_count']]
        fig = px.line(result_count, x="created_at", y="cumsum_user_subscription", color='licensed_key',
                      labels={
                          "created_at": "날짜",
                          "cumsum_user_subscription" : "일별 멤버쉽 유지 개수",
                      }
                      )

    elif graph_list == '최근 한달 판매 된 멤버십':
        fig = px.bar(month_membership, x='licensed_key', y='count', title="최근 한달 판매 된 멤버십", color='licensed_key',
                     labels={
                         "item_name": "패키지 이름"}
                     )

    elif graph_list == '최근 한달 판매 된 패키지':
        fig = px.bar(month_package, x='item_name', y='count', title="최근 한달 판매 된 패키지", color='item_name',
                     labels={
                         "item_name": "패키지 이름" }
                     )

    return fig

