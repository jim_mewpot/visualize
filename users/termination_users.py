import dash_core_components as dcc
import dash_html_components as html
from user_subscription_data import userSubscipritonData
import numpy as np
import dash_table
import datetime
import pandas as pd
from database.database import db

read_user_subscription = userSubscipritonData(db)

read_user_subscription['subscription_month'] = ((read_user_subscription['end_date'] - read_user_subscription['created_at']) / np.timedelta64(1,'M')).round(0)
grouped_user_subscription = read_user_subscription[read_user_subscription['price'] != 0].groupby(
    ['user_id', 'licensed_key']).agg({
    'price': 'sum',
    'end_date': 'max',
    'created_at': 'min',
    'subscription_month': 'sum'}).reset_index()


BEFORE_3_DAYS = datetime.datetime.now()- datetime.timedelta(days=3)
current_Termination_user = grouped_user_subscription[grouped_user_subscription['end_date']< BEFORE_3_DAYS]

SQL = f"""
SELECT 
	users.id,
    users.username
	,date_trunc('day', users.created_at) as created_at
	,users.email
	,users.platform_usage ->'video' AS video
	,users.platform_usage->'platform'->'youtube' AS youtube
	,users.platform_usage->'platform'->'instagram' AS instagram
	,users.platform_usage->'platform'->'facebook' AS facebook
	,users.platform_usage->'platform'->'others' AS others
FROM users
    """
Termination_users = pd.read_sql(SQL, db)
Termination_user_data = Termination_users.merge(grouped_user_subscription, left_on = 'id', right_on ='user_id', how = 'right')
Termination_user_data.rename(columns = {'licensed_key' : 'membership',
                           'end_date' : 'subscription_end_date',
                            'price' : 'sales',
                           'subscription_month' : 'subscription_total_month',
                           'created_at_y' : 'subscription_start_date',
                           'created_at_x' : 'id_created_at'}, inplace = True)
Termination_user_data = Termination_user_data.iloc[:,[0,1,3,4,5,6,7,8,10,11,12,13,14]]
Termination_user_data = Termination_user_data[['id', 'username', 'email', 'membership', 'sales', 'subscription_end_date',
       'subscription_start_date', 'subscription_total_month','video','youtube', 'instagram', 'facebook','others']].set_index('id')
Termination_user_data.rename(columns={'username': '닉네임',
                          'email': 'email',
                          'membership': 'membership',
                          'sales': '매출',
                          'subscription_start_date': '최초구독일',
                          'subscription_end_date': '마지막 구독일',
                          'subscription_total_month' : '구독 개월 수'}, inplace=True)

total_Termination_users = current_Termination_user['user_id'].to_list()
termination_users_df = Termination_user_data.loc[total_Termination_users,:].reset_index()
termination_users_df['video'] = termination_users_df['video'].apply(lambda x: str(x)[1:-1] if x != None else x)

layout4 =  html.Div(children=[
    html.H1(children='과거 해지고객 목록'),
    dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in termination_users_df.columns],
    data=termination_users_df.to_dict('records'),
    export_format='xlsx',
    export_headers='display',
    filter_action='native',
    sort_action='native'
    ),
    html.Br(),
    dcc.Link('이전 페이지로 이동', href='/users'),
    html.Br(),
    dcc.Link('home', href='/')
    ])