import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from user_subscription_data import userSubscipritonData
import plotly.express as px
from database.database import db
from main import app
import datetime

read_user_subscription = userSubscipritonData(db)


day_select_user_subscription_cumsum_layout = html.Div([
dcc.DatePickerRange(
        id='day_select_user_subscription_cumsum',  # ID to be used for callback
    min_date_allowed=read_user_subscription['created_at'].min().date(),
    max_date_allowed=read_user_subscription['created_at'].max().date(),
    initial_visible_month=datetime.datetime.today().date(),
    end_date=read_user_subscription['created_at'].max().date()
    ),
    dcc.Graph(id='day_select_user_subscription_cumsum_graph'),
    html.Br(),
    dcc.Link('이전 페이지로 이동', href='/users'),
    html.Br(),
    dcc.Link('home', href='/'),
])

@app.callback(
    Output('day_select_user_subscription_cumsum_graph', 'figure'),
    [Input('day_select_user_subscription_cumsum', 'start_date'),
     Input('day_select_user_subscription_cumsum', 'end_date')]
)

def drawUserSubsciptionCumsum(start_date, end_date):
    start_count = read_user_subscription.loc[:, ['user_id', 'created_at', 'licensed_key']].groupby(
        ['created_at', 'licensed_key']).count().reset_index()
    start_count.rename(columns={'user_id': 'start_count'}, inplace=True)
    end_count = read_user_subscription.loc[:, ['user_id', 'end_date', 'licensed_key']].groupby(
        ['end_date', 'licensed_key']).count().reset_index()
    end_count.rename(columns={'user_id': 'end_count'}, inplace=True)
    result_count = start_count.merge(end_count, left_on=['licensed_key', 'created_at'],
                                     right_on=['licensed_key', 'end_date'], how='left').iloc[:,
                   [0, 1, 2, 4]].fillna(0)
    result_count['difference_count'] = result_count['start_count'] - result_count['end_count']
    result_count['cumsum_user_subscription'] = result_count.groupby('licensed_key').cumsum().loc[:,
                                               ['difference_count']]
    selected_data = result_count.set_index('created_at').loc[start_date:end_date].reset_index()
    fig = px.line(selected_data, x="created_at", y="cumsum_user_subscription", color='licensed_key',
                  labels={
                      "created_at": "날짜",
                      "cumsum_user_subscription": "일별 멤버쉽 유지 개수",})
    fig.update_layout(width=1400, height=700)
    return fig