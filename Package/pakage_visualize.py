import dash
import dash_html_components as html
import dash_core_components as dcc
from main import app
import pandas as pd
from datetime import timedelta
import os
import plotly.offline as pyo
import plotly.express as px
import plotly.graph_objs as go
from plotly.subplots import make_subplots
os.chdir('../')
from database.database import db

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

SQL = f"""
SELECT item_name, count(user_id) as pay_count, sum(price),month_created_at
FROM (
	SELECT user_id, price, date_trunc('month',created_at) as month_created_at , item_name
	FROM payment_approved_items
	WHERE item_type = 'Classify::Package'
	ANd refund_id IS null
	AND price != 0) as package_download
GROUP BY item_name, month_created_at
    """
user_package_downloads= pd.read_sql(SQL, db)

pakage_visualize_layout = html.Div([
    dcc.Dropdown(
        id='pakage_visualize_graph_dropdown',
        options=[
            {'label': '패키지별 월별 구매 개수', 'value': '패키지별 월별 구매 개수'},
            {'label': '월별 전체 패키지 수익', 'value': '월별 전체 패키지 수익'},
            {'label': '패키지별 누적 구매 개수', 'value': '패키지별 누적 구매 개수'}
        ],
    ),
    dcc.Graph(id='pakage_visualize_graph')
])


@app.callback(
    dash.dependencies.Output('pakage_visualize_graph', 'figure'),
    [dash.dependencies.Input('pakage_visualize_graph_dropdown', 'value')])



def update_output(value):
    if value == '패키지별 월별 구매 개수':
        fig = px.line(user_package_downloads, x="month_created_at", y="pay_count", color='item_name',
                      title="패키지별 월별 구매 개수")
        fig.update_layout(width=1400, height=700)
    elif value == '월별 전체 패키지 수익':
        classify_packages_title = pd.read_sql(
            "SELECT title, date_trunc('month',created_at) as created_at FROM classify_packages", db)
        grouped_created_at_title = classify_packages_title.groupby('created_at').count().reset_index()

        # 월별 수익
        month_sum_price = user_package_downloads.loc[:, ['month_created_at', 'sum']].groupby(
            'month_created_at').sum().reset_index()
        len_graph_row = len(month_sum_price)
        max_idx = month_sum_price['sum'].idxmax()

        colors = ['lightslategray', ] * len_graph_row
        colors[max_idx] = 'crimson'

        fig = make_subplots(specs=[[{"secondary_y": True}]])

        fig.add_trace(
            go.Bar(
                x=month_sum_price['month_created_at'].to_list()
                , y=month_sum_price['sum'].to_list(), name="패키지 월별 매출",
                marker_color=colors),
            secondary_y=False, )
        # fig.add_line(x=grouped_created_at_title['created_at'].to_list(), y=grouped_created_at_title['title'].to_list())

        fig.add_trace(
            go.Scatter(x=grouped_created_at_title['created_at'].to_list(),
                       y=grouped_created_at_title['title'].to_list(),
                       name="패키지 출시 개수"),
            secondary_y=True,
        )

        fig.update_yaxes(title_text="패키지 월별 매출", secondary_y=False)
        fig.update_yaxes(title_text="패키지 출시 개수", secondary_y=True)

        fig.update_layout(title_text='월별 전체 패키지 수익',
                          width=1400, height=700)

    elif value == '패키지별 누적 구매 개수':
        SQL = f"""
        SELECT item_name, count(user_id) as pay_count, sum(price),week_created_at
        FROM (
        	SELECT user_id, price, date_trunc('week',created_at) as week_created_at , item_name
        	FROM payment_approved_items
        	WHERE item_type = 'Classify::Package'
        	ANd refund_id IS null
        	AND price != 0) as package_download
        GROUP BY item_name, week_created_at
            """
        user_package_downloads_week = pd.read_sql(SQL, db)
        user_package_downloads_week['pay_count_cumsum'] = user_package_downloads_week.groupby('item_name').cumsum()[
            'pay_count']
        fig = px.line(user_package_downloads_week, x="week_created_at", y="pay_count_cumsum", color='item_name',
                      title="패키지별 누적 구매 개수")

        fig.update_yaxes(title_text="누적 판매 개수")
        fig.update_xaxes(title_text="날짜")
        fig.update_layout(width=1400, height=700)
    return fig