import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from main import app
from users.users import app_layout
from users.daily_subscribtion import layout1
from users.change_memvership import layout2
from users.VIP import layout3
from users.termination_users import layout4
from users.day_select_user_subscription import day_select_user_subscription_layout
from users.day_select_user_package import day_select_user_package_layout
from main import main_layout
from users.day_slect_user_subscription_cumsum import day_select_user_subscription_cumsum_layout
from Package.pakage_visualize import  pakage_visualize_layout
import dash_auth

VALID_USERNAME_PASSWORD_PAIRS = {
    'mewpot': 'Mewmew43!'
}

auth = dash_auth.BasicAuth(
    app,
    VALID_USERNAME_PASSWORD_PAIRS
)



app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])

@app.callback(Output('page-content', 'children'),
              Input('url', 'pathname'))
def display_page(pathname):
    if pathname == '/users/daily_subscribtion':
         return day_select_user_subscription_cumsum_layout
    elif pathname == '/users/change_memvership':
         return layout2
    elif pathname == '/users/VIP':
         return layout3
    elif pathname == '/users':
        return app_layout
    elif pathname == '/users/termination':
        return layout4
    elif pathname =='/users/day_select_user_subscription':
        return day_select_user_subscription_layout
    elif pathname =='/users/day_select_user_package':
        return day_select_user_package_layout
    elif pathname =='/':
        return main_layout
    elif pathname =='/package':
        return pakage_visualize_layout
    else:
        return '404'

if __name__ == '__main__':
    app.run_server(host = '0.0.0.0',port=8889, debug=False)
