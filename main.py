import dash
import dash_html_components as html
import dash_core_components as dcc

app = dash.Dash(__name__, suppress_callback_exceptions=True)
server = app.server

main_layout =  html.Div(children=[
    html.H3('Mewpot'),
    html.Br(),
    dcc.Link('Users', href='/users'),
    html.Br(),
    dcc.Link('Package', href = '/package')
])