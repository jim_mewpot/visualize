import os
import sqlalchemy as db
import pandas as pd


class DB:
    def connect(self, status, debug):
        if debug == True:
            echo = True
        elif debug == False:
            echo = False

        if status == 'server':
            host = os.environ["DB_HOST"]
            user = 'mewpot_ai'
            password = 'Mewmew43!'
            db_name = 'mewpot'
            port = 5432
        elif status == 'local':
            host = "127.0.0.1"
            user = 'postgres'
            password = os.environ["LOCAL_PASSWORD"]
            db_name = 'mewpot'
            port = 5432

        elif status == 'mac_local':
            host = "127.0.0.1"
            user = 'jin0'
            password = os.environ["LOCAL_PASSWORD"]
            db_name = 'mewpot'
            port = 5432

        elif status == 'kevin_local':
            host = "121.133.58.231"
            user = 'mewpot'
            password = 'kkkkkk'
            port = 5432

        else:
            print('Please input local or server')

        url = 'postgresql://{}:{}@{}:{}/{}'.format(user, password, host, port, db_name)
 
        self.engine = db.create_engine(url, client_encoding='utf8', echo=echo)
        self.connection = self.engine.connect()
        self.metadata = db.MetaData()

        return self.engine, self.connection, self.metadata

    def read_db(self, tablename):
        # select table
        table = db.Table(tablename, self.metadata, autoload=True, autoload_with=self.engine)
        return table

    def read_sql(self, query):
        # read query
        ResultProxy = self.connection.execute(query)
        ResultSet = ResultProxy.fetchall()

        # to dataframe
        result_df = pd.DataFrame(ResultSet)
        result_df.columns = ResultSet[0].keys()

        return result_df

    def upload_db(self, df, tablename):
        df.to_sql(tablename, con=self.engine, if_exists='append', chunksize=1000, index=False)

    def delete_db(self, sql):
        self.engine.execute(sql)

database = DB()
db = database.connect('server', debug=False)[1]
